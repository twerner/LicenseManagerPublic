using LicenseManager.Shared.Models;

namespace LicenseManager.API.Migrations
{
    using Shared.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.SqlTypes;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            context.Manufacturers.AddOrUpdate(
                m => m.Id,
                new Manufacturer() { Id = 1, Name = "Microsoft" }
            );

            context.Softwares.AddOrUpdate(
                s => s.Id,
                new Software()
                {
                    Id = 1,
                    ManufacturerId = 1,
                    Name = "Windows 8",
                    Genre = "OS",
                    Description = "",
                    Created = DateTime.Now,
                    LastModified = DateTime.Now
                }
            );

            context.Licenses.AddOrUpdate(
                l => l.Id,
                new License()
                {
                    Id = 1,
                    SoftwareId = 1,
                    Key = "TEST",
                    Edition = "Professional",
                    Created = DateTime.Now,
                    LastModified = DateTime.Now
                });


        }
    }
}
