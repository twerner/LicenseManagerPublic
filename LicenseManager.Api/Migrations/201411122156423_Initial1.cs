namespace LicenseManager.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Licenses", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Licenses", "LastModifiedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Licenses", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Licenses", new[] { "LastModifiedBy_Id" });
            AddColumn("dbo.Licenses", "CreatedBy", c => c.String());
            DropColumn("dbo.Licenses", "LastModified");
            DropColumn("dbo.Licenses", "Version");
            DropColumn("dbo.Licenses", "CreatedBy_Id");
            DropColumn("dbo.Licenses", "LastModifiedBy_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Licenses", "LastModifiedBy_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Licenses", "CreatedBy_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Licenses", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Licenses", "LastModified", c => c.DateTime(nullable: false));
            DropColumn("dbo.Licenses", "CreatedBy");
            CreateIndex("dbo.Licenses", "LastModifiedBy_Id");
            CreateIndex("dbo.Licenses", "CreatedBy_Id");
            AddForeignKey("dbo.Licenses", "LastModifiedBy_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Licenses", "CreatedBy_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
