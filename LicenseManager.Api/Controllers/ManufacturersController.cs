﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LicenseManager.Shared.Models;
using NLog;

namespace LicenseManager.API.Controllers
{
    [Authorize]
    public class ManufacturersController : ApiController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        
        // GET: api/Manufacturers
        [AllowAnonymous]
        public IEnumerable<Manufacturer> GetManufacturers()
        {
            return _db.Manufacturers.ToList();
        }

        // GET: api/Manufacturers/5
        [AllowAnonymous]
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> GetManufacturer(int id)
        {
            Manufacturer manufacturer = _db.Manufacturers.Single(m => m.Id.Equals(id));
            if (manufacturer == null)
            {
                return NotFound();
            }

            return Ok(manufacturer);
        }

        // PUT: api/Manufacturers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutManufacturer(int id, Manufacturer manufacturer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != manufacturer.Id)
            {
                return BadRequest();
            }

            _db.Entry(manufacturer).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ManufacturerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Manufacturers
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> PostManufacturer(Manufacturer manufacturer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (ManufacturerExists(manufacturer.Name))
            {
                return BadRequest("manufacturer already exists");
            }

            _db.Manufacturers.Add(manufacturer);
            await _db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = manufacturer.Id }, manufacturer);
        }

        // DELETE: api/Manufacturers/5
        [ResponseType(typeof(Manufacturer))]
        public async Task<IHttpActionResult> DeleteManufacturer(int id)
        {
            Manufacturer manufacturer = await _db.Manufacturers.FindAsync(id);
            if (manufacturer == null)
            {
                return NotFound();
            }

            _db.Manufacturers.Remove(manufacturer);
            await _db.SaveChangesAsync();

            return Ok(manufacturer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ManufacturerExists(int id)
        {
            return _db.Manufacturers.Count(e => e.Id == id) > 0;
        }

        private bool ManufacturerExists(string name)
        {
            return _db.Manufacturers.Count(e => e.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)) > 0;
        }
    }
}