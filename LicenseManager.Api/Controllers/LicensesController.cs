﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LicenseManager.Shared.Models;
using Microsoft.AspNet.Identity;
using NLog;
using WebGrease.Activities;

namespace LicenseManager.API.Controllers
{
    [Authorize]
    public class LicensesController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private ApplicationDbContext _db = new ApplicationDbContext();

        // GET: api/Licenses
        public IQueryable<LicenseDto> GetLicenses()
        {
            var licenses = from l in _db.Licenses
                select new LicenseDto()
                {
                    Id = l.Id,
                    SoftwareId = l.SoftwareId,
                    Comment = l.Comment,
                    Edition = l.Edition,
                    Key = l.Key,
                    UsedOn = l.UsedOn
                };
            return licenses;
        }

        // GET: api/Licenses/5
        [ResponseType(typeof(License))]
        public async Task<IHttpActionResult> GetLicense(int id)
        {
            var license = await _db.Licenses
                .Include(l => l.UsedOn)
                .SingleOrDefaultAsync(l => l.Id == id);

            if (license == null)
            {
                return NotFound();
            }

            return Ok(license);
        }

        // PUT: api/Licenses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLicense(int id, License license)
        {
            if (!ModelState.IsValid)
            {
                Logger.Debug("invalid model state");
                return BadRequest(ModelState);
            }

            if (id != license.Id)
            {
                return BadRequest();
            }

            var currentUserId = User.Identity.GetUserId();
            license.LastModified = DateTime.Now;
            license.LastModifiedBy = _db.Users.Find(currentUserId).Id;
            license.Version++;


            _db.Entry(license).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                Logger.Error(e);
                if (!LicenseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Licenses
        [ResponseType(typeof(License))]
        public async Task<IHttpActionResult> PostLicense(License license)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (LicenseExists(license.SoftwareId, license.Key))
            {
				Logger.Error("license already exists: {0}", license);
                return BadRequest("license already exists");
            }

            var currentUserId = User.Identity.GetUserId();

            license.Created = DateTime.Now;
            license.CreatedBy = _db.Users.Find(currentUserId).Id;
            license.LastModified = license.Created;
            license.LastModifiedBy = license.CreatedBy;
            license.Version = 1;

            Logger.Debug("try to save changes now...");

            try
            {
                _db.Licenses.Add(license);
                await _db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw ex;
            }

            return CreatedAtRoute("DefaultApi", new { id = license.Id }, license);
        }

        // DELETE: api/Licenses/5
        [ResponseType(typeof(License))]
        public async Task<IHttpActionResult> DeleteLicense(int id)
        {
            License license = await _db.Licenses.FindAsync(id);
            if (license == null)
            {
                return NotFound();
            }

            _db.Licenses.Remove(license);
            await _db.SaveChangesAsync();

            return Ok(license);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LicenseExists(int id)
        {
            return _db.Licenses.Count(e => e.Id == id) > 0;
        }

        private bool LicenseExists(int softwareId, string key)
        {
            return _db.Licenses.Count(e => e.SoftwareId == softwareId &&
                e.Key.Equals(key)) > 0;
        }
    }
}