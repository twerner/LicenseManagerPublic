﻿using LicenseManager.Shared.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace LicenseManager.API.Controllers
{
    [Authorize]
    public class SoftwaresController : ApiController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        // GET: api/Softwares
        [AllowAnonymous]
        public IQueryable<SoftwareDto> GetSoftwares()
        {
            var softwares = from s in _db.Softwares
                            select new SoftwareDto()
                            {
                                Id = s.Id,
                                Name = s.Name,
                                ManufacturerName = s.Manufacturer.Name,
                                Genre = s.Genre,
                                Created = s.Created,
                                CreatedById = s.CreatedBy.Id,
                                CreatedByUserName = s.CreatedBy.UserName
                            };
            return softwares;
        }

        // GET: api/Softwares/5
        [ResponseType(typeof(SoftwareDetailDto))]
        public async Task<IHttpActionResult> GetSoftware(int id)
        {
            var software = await _db.Softwares.Include(s => s.Manufacturer)
                .Include(s => s.CreatedBy).Include(s => s.LastModifiedBy)
                .Select(s => new SoftwareDetailDto()
                {
                    Id = s.Id,
                    Name = s.Name,
                    ManufacturerId = s.ManufacturerId,
                    ManufacturerName = s.Manufacturer.Name,
                    Genre = s.Genre,
                    Description = s.Description,
                    Created = s.Created,
                    CreatedById = s.CreatedBy.Id,
                    CreatedByUserName = s.CreatedBy.UserName,
                    LastModified = s.LastModified,
                    LastModifiedById = s.LastModifiedBy.Id,
                    LastModifiedByUserName = s.LastModifiedBy.UserName,
                    Licenses = s.Licenses,
                    Version = s.Version
                }).SingleOrDefaultAsync(s => s.Id == id);

            if (software == null)
            {
                Logger.Error("Software with id={0} not found.", id);
                return NotFound();
            }

            return Ok(software);
        }

        //GET: api/Softwares/5/Licenses
        [Route("api/Softwares/{id}/Licenses")]
        [HttpGet]
        [ResponseType(typeof(ICollection<License>))]
        public async Task<IHttpActionResult> GetLicensesBySoftware(int id)
        {
            var software = await _db.Softwares.SingleOrDefaultAsync(s => s.Id == id);

            if (null == software)
            {
                Logger.Error("Software with id={0} not found.", id);
                return NotFound();
            }
            
            return Ok(software.Licenses);
        }

        // PUT: api/Softwares/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSoftware(int id, Software software)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != software.Id)
            {
                return BadRequest();
            }

            var currentUserId = User.Identity.GetUserId();
            software.LastModified = DateTime.Now;
            software.LastModifiedBy = _db.Users.Find(currentUserId);
            software.Version++;

            _db.Entry(software).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SoftwareExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Softwares
        [ResponseType(typeof(Software))]
        public async Task<IHttpActionResult> PostSoftware(Software software)
        {
            if (!ModelState.IsValid)
            {
                Logger.Error("Model state is not valid {0}", software);
                return BadRequest(ModelState);
            }

            if (SoftwareExists(software.ManufacturerId, software.Name))
            {
                Logger.Error("software already exists: {0}", software);
                return BadRequest("software already exists");
            }

            var currentUserId = User.Identity.GetUserId();

            software.Created = DateTime.Now;
            software.LastModified = software.Created;
            software.CreatedBy = _db.Users.Find(currentUserId);
            software.LastModifiedBy = software.CreatedBy;
            software.Version = 1;

            try
            {
                _db.Softwares.Add(software);
                await _db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw ex;
            }

            return CreatedAtRoute("DefaultApi", new { id = software.Id }, software);
        }

        // DELETE: api/Softwares/5
        [ResponseType(typeof(Software))]
        public async Task<IHttpActionResult> DeleteSoftware(int id)
        {
            Software software = await _db.Softwares.FindAsync(id);
            if (software == null)
            {
                return NotFound();
            }

            _db.Softwares.Remove(software);
            await _db.SaveChangesAsync();

            return Ok(software);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SoftwareExists(int id)
        {
            return _db.Softwares.Count(e => e.Id == id) > 0;
        }

        private bool SoftwareExists(int manufacturerId, string name)
        {
            return _db.Softwares.Count(e => e.ManufacturerId == manufacturerId
                &&e.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)) > 0;
        }
    }
}