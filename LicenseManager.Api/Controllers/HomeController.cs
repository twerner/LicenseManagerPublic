﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using NLog;
using System.Reflection;

namespace LicenseManager.API.Controllers
{
    public class HomeController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.Version = Assembly.GetExecutingAssembly().GetName().Version;
            return View();
        }
    }
}
