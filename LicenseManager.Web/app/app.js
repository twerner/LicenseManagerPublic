﻿var app = angular.module('LicenseManager', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

app.config(function($routeProvider) {
    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "app/views/home.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "app/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "app/views/signup.html"
    });

    $routeProvider.when("/softwares", {
        controller: "softwaresController",
        templateUrl: "app/views/softwares.html"
    });

    $routeProvider.otherwise({ redirectTo: "/home" });
});

//var serviceBase = "http://localhost:2289/";
var serviceBase = "http://lm.torsten-werner.com/api/";
app.constant('lmApiSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'LmWebApp'
});

app.run([
    'authService', function(authService) {
        authService.fillAuthData();
    }
]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});