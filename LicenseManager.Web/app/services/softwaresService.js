﻿'use strict';
app.factory('softwaresService', ['$http', 'lmApiSettings', function ($http, lmApiSettings) {

    //var serviceBase = 'http://localhost:2289/';
    var serviceBase = lmApiSettings.apiServiceBaseUri;
    var softwaresServiceFactory = {};

    var _getSoftwares = function() {
        return $http.get(serviceBase + 'api/softwares').then(function (results) {
            return results;
        });
    };

    softwaresServiceFactory.getSoftwares = _getSoftwares;

    return softwaresServiceFactory;
}]);