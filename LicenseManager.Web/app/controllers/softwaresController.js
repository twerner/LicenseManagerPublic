﻿'use strict';
app.controller('softwaresController', [
    '$scope', 'softwaresService', function ($scope, softwaresService) {
        $scope.softwares = [];
        softwaresService.getSoftwares().then(function(results) {
            $scope.softwares = results.data;
        }, function(error) {
            alert(error.data.message);
        });
    }
]);