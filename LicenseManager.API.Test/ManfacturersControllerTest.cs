﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LicenseManager.API.Models;
using LicenseManager.API.Controllers;
using System.Collections.Generic;

namespace LicenseManager.API.Test
{
    [TestClass]
    public class ManfacturersControllerTest
    {
        [TestMethod]
        public void GetManufacturers_ShouldReturnAllManufacturers()
        {
            var manufacturers = GetManufacturers();
            var controller = new ManufacturersController(manufacturers);

            var result = controller.GetManufacturers() as IEnumerable<Manufacturer>;
            // TODO Assert.AreEqual(manufacturers.Count, result.Count)
            //Assert.AreEqual(manufacturers.GetAll().GetEnumerator().
        }

        private IManufacturerRepository GetManufacturers()
        {
            ManufacturerRepository manufacturers = new ManufacturerRepository();
            manufacturers.Add(new Manufacturer { Name = "Manufacturer1" });
            manufacturers.Add(new Manufacturer { Name = "Manufacturer2" });
            manufacturers.Add(new Manufacturer { Name = "Manufacturer3" });

            return manufacturers;
        }
    }
}
