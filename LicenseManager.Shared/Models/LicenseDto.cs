﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// License data transfer object.
    /// </summary>
    public class LicenseDto
    {
        /// <summary>
        /// License-Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Software Foreign Key
        /// </summary>
        public int SoftwareId { get; set; }
        /// <summary>
        /// The Activation Key.
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Software Edition
        /// </summary>
        /// <example>Professional if the Software is Windows 7.</example>
        public string Edition { get; set; }
        /// <summary>
        /// Something special
        /// </summary>
        /// <example>English license</example>
        public string Comment { get; set; }

        /// <summary>
        /// Hostname of the computer(s) the license is used on.
        /// </summary>
        public string UsedOn { get; set; }
    }
}