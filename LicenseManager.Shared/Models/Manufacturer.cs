﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// Manufacturer.
    /// </summary>
    public class Manufacturer
    {
        /// <summary>
        /// Manufacturer id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Manufacturer name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Collection of softwares by this manufacturer.
        /// </summary>
        public ICollection<Software> Softwares { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}