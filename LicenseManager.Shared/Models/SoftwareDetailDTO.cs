﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// Software Detail Data Transfer Object
    /// </summary>
    public class SoftwareDetailDto
    {
        /// <summary>
        /// Software id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Software name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Manufacturer id.
        /// </summary>
        public int ManufacturerId { get; set; }
        /// <summary>
        /// Manufacturer name.
        /// </summary>
        public string ManufacturerName { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Creation date.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Last modification date.
        /// </summary>
        public DateTime LastModified { get; set; }
        /// <summary>
        /// Created by user id.
        /// </summary>
        public string CreatedById { get; set; }
        /// <summary>
        /// Created by user name.
        /// </summary>
        public string CreatedByUserName { get; set; }
        /// <summary>
        /// Last modified by user id.
        /// </summary>
        public string LastModifiedById { get; set; }
        /// <summary>
        /// Last modified by user name.
        /// </summary>
        public string LastModifiedByUserName { get; set; }
        /// <summary>
        /// Version.
        /// </summary>
        public int Version { get; set; }
        /// <summary>
        /// Licenses for this software.
        /// </summary>
        public ICollection<License> Licenses { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}", ManufacturerName, Name);  
        }
    }
}