﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// Software Data Transfer Object.
    /// </summary>
    public class SoftwareDto
    {
        /// <summary>
        /// Software id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Software name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Manufacturer Name.
        /// </summary>
        public string ManufacturerName { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Creation date.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Created by user id.
        /// </summary>
        public string CreatedById { get; set; }
        /// <summary>
        /// Created by user name.
        /// </summary>
        public string CreatedByUserName { get; set; }
    }
}