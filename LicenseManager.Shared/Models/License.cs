﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// One License for a software.
    /// </summary>
    public class License
    {
        /// <summary>
        /// License-Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Software Foreign Key
        /// </summary>
        public int SoftwareId { get; set; }
        /// <summary>
        /// The Activation Key.
        /// </summary>
        [Required]
        public string Key { get; set; }
        /// <summary>
        /// Software Edition
        /// </summary>
        /// <example>Professional if the Software is Windows 7.</example>
        public string Edition { get; set; }
        /// <summary>
        /// Something special
        /// </summary>
        /// <example>English license</example>
        public string Comment { get; set; }
        /// <summary>
        /// Date, when this entry is created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// User-Id who created this entry.
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Date of last modification.
        /// </summary>
        public DateTime LastModified { get; set; }
        
        /// <summary>
        /// User-Id of last modification.
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Version.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Hostname of the computer(s) the license is used on.
        /// </summary>
        public string UsedOn { get; set; }
        
    }
}