﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LicenseManager.Shared.Models
{
    /// <summary>
    /// Software.
    /// </summary>
    public class Software
    {
        /// <summary>
        /// Software id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Manufacturer id.
        /// </summary>
        public int ManufacturerId { get; set; }
        /// <summary>
        /// Name of this software.
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Creation date.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Last modification date.
        /// </summary>
        public DateTime LastModified { get; set; }
        /// <summary>
        /// Version.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Manufacturer.
        /// </summary>
        public virtual Manufacturer Manufacturer { get; set; }
        /// <summary>
        /// Licenses for this software.
        /// </summary>
        public virtual ICollection<License> Licenses { get; set; }

        /// <summary>
        /// Created by user id.
        /// </summary>
        public virtual ApplicationUser CreatedBy { get; set; }
        /// <summary>
        /// Last modified by user id.
        /// </summary>
        public virtual ApplicationUser LastModifiedBy { get; set; }


        public override string ToString()
        {
            return String.Format("{0} - {1}", Manufacturer.Name, Name);
        }
    }
}