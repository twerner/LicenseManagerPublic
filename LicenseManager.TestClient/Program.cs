﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LicenseManager.Shared.Models;
using LicenseManager.TestClient.Models;

namespace LicenseManager.TestClient
{
    class Program
    {
        private static TokenModel tokenModel;
        private static string baseAddress = "http://localhost:2289/";

        static void Main(string[] args)
        {
            RegisterUser().Wait();
            LoginTest().Wait();
            //PostLicenseTest().Wait();
            GetData().Wait();
            Console.ReadKey();
        }

        static async Task RegisterUser()
        {
            RegisterModel register = new RegisterModel()
            {
                ConfirmPassword = "Test123;",
                Email = "tw@to-wer.de",
                Password = "Test123;"
            };

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsJsonAsync("api/Account/Register", register);
                var responseMessage = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseMessage);
            }
        }

        static async Task PostLicenseTest()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", tokenModel.AccessToken));

                License license = new License()
                {
                    SoftwareId = 1,
                    Comment = "",
                    Edition = "Pro",
                    Key = "TESTLICENSE4",

                };

                HttpResponseMessage response = await client.PostAsJsonAsync("api/licenses", license);
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);

            }
        }

        static async Task LoginTest()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsync("Token", new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", "tw@to-wer.de"),
                    new KeyValuePair<string, string>("password", "Test123;")
                }));

                if (!response.IsSuccessStatusCode)
                {
                    return;
                }

                tokenModel = await response.Content.ReadAsAsync<TokenModel>();
                Console.WriteLine("Login OK");
            }
        }

        private static async Task GetData()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", tokenModel.AccessToken));

                var response = await client.GetAsync("api/Softwares");
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
            }
        }

        private static async Task InsertSoftware()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", tokenModel.AccessToken));

                Software sw = new Software()
                {
                    Description = "",
                    Genre = "Betriebssystem",
                    ManufacturerId = 1,
                    Name = "Windows 8",

                };

                var response = await client.PostAsJsonAsync("api/softwares", sw);
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
            }
        }
    }

    internal class RegisterModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
