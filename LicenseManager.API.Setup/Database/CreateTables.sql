﻿USE [master]
GO
/****** Object:  Database [LicenseManagerApi]    Script Date: 16.01.2015 22:33:05 ******/
CREATE DATABASE [LicenseManagerApi]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LicenseManagerApi', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LicenseManagerApi.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LicenseManagerApi_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LicenseManagerApi_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LicenseManagerApi] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LicenseManagerApi].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LicenseManagerApi] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET ARITHABORT OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LicenseManagerApi] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LicenseManagerApi] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LicenseManagerApi] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LicenseManagerApi] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LicenseManagerApi] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET RECOVERY FULL 
GO
ALTER DATABASE [LicenseManagerApi] SET  MULTI_USER 
GO
ALTER DATABASE [LicenseManagerApi] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LicenseManagerApi] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LicenseManagerApi] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LicenseManagerApi] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [LicenseManagerApi]
GO
/****** Object:  User [LmUser]    Script Date: 16.01.2015 22:33:05 ******/
CREATE USER [LmUser] FOR LOGIN [LmUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_datareader] ADD MEMBER [LmUser]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [LmUser]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 16.01.2015 22:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Licenses]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Licenses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SoftwareId] [int] NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Edition] [nvarchar](max) NULL,
	[Comment] [nvarchar](max) NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UsedOn] [nvarchar](max) NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](max) NULL,
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Licenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manufacturers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Manufacturers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Softwares]    Script Date: 16.01.2015 22:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Softwares](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ManufacturerId] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Genre] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Created] [datetime] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedBy_Id] [nvarchar](128) NULL,
	[LastModifiedBy_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.Softwares] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 16.01.2015 22:33:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 16.01.2015 22:33:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SoftwareId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_SoftwareId] ON [dbo].[Licenses]
(
	[SoftwareId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedBy_Id]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedBy_Id] ON [dbo].[Softwares]
(
	[CreatedBy_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_LastModifiedBy_Id]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_LastModifiedBy_Id] ON [dbo].[Softwares]
(
	[LastModifiedBy_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ManufacturerId]    Script Date: 16.01.2015 22:33:06 ******/
CREATE NONCLUSTERED INDEX [IX_ManufacturerId] ON [dbo].[Softwares]
(
	[ManufacturerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Licenses] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [LastModified]
GO
ALTER TABLE [dbo].[Licenses] ADD  DEFAULT ((0)) FOR [Version]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Licenses]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Licenses_dbo.Softwares_SoftwareId] FOREIGN KEY([SoftwareId])
REFERENCES [dbo].[Softwares] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Licenses] CHECK CONSTRAINT [FK_dbo.Licenses_dbo.Softwares_SoftwareId]
GO
ALTER TABLE [dbo].[Softwares]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Softwares_dbo.AspNetUsers_CreatedBy_Id] FOREIGN KEY([CreatedBy_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Softwares] CHECK CONSTRAINT [FK_dbo.Softwares_dbo.AspNetUsers_CreatedBy_Id]
GO
ALTER TABLE [dbo].[Softwares]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Softwares_dbo.AspNetUsers_LastModifiedBy_Id] FOREIGN KEY([LastModifiedBy_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Softwares] CHECK CONSTRAINT [FK_dbo.Softwares_dbo.AspNetUsers_LastModifiedBy_Id]
GO
ALTER TABLE [dbo].[Softwares]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Softwares_dbo.Manufacturers_ManufacturerId] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Softwares] CHECK CONSTRAINT [FK_dbo.Softwares_dbo.Manufacturers_ManufacturerId]
GO
USE [master]
GO
ALTER DATABASE [LicenseManagerApi] SET  READ_WRITE 
GO
