﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LicenseManager.Client.Models;

using NLog;

namespace LicenseManager.Client.Pages
{
    /// <summary>
    /// Interaktionslogik für LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();
        private string _email;
        private string _password;
        private static HttpResponseMessage _responseMessage;
        private static TokenModel _tokenModel;

        public LoginPage()
        {
            InitializeComponent();
            
            //TODO: remove before commit!!
            txtUsername.Text = "test@twerner.eu";
            txtPassword.Password = "Test123;";

            doLogin();
        }

        private async void doLogin()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));

                _tokenModel = null;
                Constants.TokenModel = null;

                _responseMessage = await client.PostAsync("Token", new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", txtUsername.Text),
                    new KeyValuePair<string, string>("password", txtPassword.Password)
                }));
                if (!_responseMessage.IsSuccessStatusCode)
                {
                    string responseError = await _responseMessage.Content.ReadAsStringAsync();
                    _logger.Error(responseError);
                    TextBoxTest.Text = responseError;
                    return;
                }
            }
            string response = await _responseMessage.Content.ReadAsStringAsync();
            _tokenModel = await _responseMessage.Content.ReadAsAsync<TokenModel>();

            if (!String.IsNullOrEmpty(_tokenModel.AccessToken))
            {
                Constants.TokenModel = _tokenModel;
                NavigateToHome();
            }
            TextBoxTest.Text = response;
        }

        private void NavigateToHome()
        {
            this.NavigationService.Navigate(new HomePage());
        }

        private void TxtPassword_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoginButton_OnClick(sender, e);
            }
        }
            
        private void LoginButton_OnClick(object sender, RoutedEventArgs e)
        {
            _email = txtUsername.Text;
            _password = txtPassword.Password;

            doLogin();
        }
    }
}
