﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using LicenseManager.Client.Dialogs;
using LicenseManager.Client.Grids;
using LicenseManager.Client.Models;
using NLog;
using MessageBox = System.Windows.MessageBox;

namespace LicenseManager.Client.Pages
{
    /// <summary>
    /// Interaktionslogik für HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private SoftwareDto[] _softwares;
        private SoftwareDetailDto _currentSoftwareDetailDto;
        private bool _newSoftware = false;
        private Grid _currentGrid;

        public HomePage()
        {
            InitializeComponent();
            Initialize();
            GetSoftwares();
        }

        private void Initialize()
        {
            WelcomeGrid welcomeGrid = new WelcomeGrid("welcomeGrid");
            Grid.SetColumn(welcomeGrid, 1);
            Grid.SetRow(welcomeGrid, 1);
            _currentGrid = welcomeGrid;
            Refresh();
        }

        private void Refresh()
        {
            ParentGrid.Children.Add(_currentGrid);
            if (_currentSoftwareDetailDto != null)
            {
                BtnNewLicense.IsEnabled = true;
                BtnNewLicenseImage.Source = new BitmapImage(
                new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/add-key-icon.png"));
            }
            else
            {
                BtnNewLicense.IsEnabled = false;
                BtnNewLicenseImage.Source = new BitmapImage(
                new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/add-key-icon_grau.png"));
            }
        }

        private async void GetSoftwares()
        {
            SoftwaresListView.Items.Clear();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync("api/softwares");
                if (!response.IsSuccessStatusCode)
                {
                    var responseError = await response.Content.ReadAsStringAsync();
                    _logger.Error(responseError);
                    return;
                }
                _softwares = await response.Content.ReadAsAsync<SoftwareDto[]>();
            }
            foreach (var item in _softwares)
            {
                SoftwaresListView.Items.Add(item);
            }
            InitializeComponent();
        }

        private async void GetSoftwareDetails()
        {
            if(null == SoftwaresListView.SelectedItem)return;
            var selectedSoftware = (SoftwareDto) SoftwaresListView.SelectedItem;
            StatusLabel.Content = "Loading software details...";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", Constants.TokenModel.AccessToken));

                var response = await client.GetAsync("api/softwares/" + selectedSoftware.Id);
                if (!response.IsSuccessStatusCode)
                {
                    var responseError = await response.Content.ReadAsStringAsync();
                    _logger.Error(responseError);
                    return;
                }
                _currentSoftwareDetailDto = await response.Content.ReadAsAsync<SoftwareDetailDto>();
            }
            SoftwareBaseGrid swDetailGrid = new SoftwareBaseGrid(_currentSoftwareDetailDto);
            Grid.SetColumn(swDetailGrid, 1);
            Grid.SetRow(swDetailGrid, 1);
            ParentGrid.Children.Remove(_currentGrid);
            _currentGrid = swDetailGrid;
            Refresh();
            StatusLabel.Content = "Software details loaded successfully." + DateTime.Now.ToString();
        }


        private void SoftwaresListView_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetSoftwareDetails();
        }

        private void DeleteSwButton_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //TODO Dialog ob wirklich gelöscht werden soll
            
            //configure message box
            string messageboxText = "Do you really want to delete this software?";
            string caption = "Sure?";
            MessageBoxButton button = MessageBoxButton.YesNo;
            MessageBoxImage icon = MessageBoxImage.Asterisk;

            MessageBoxResult result = MessageBox.Show(messageboxText, caption, button, icon);

            if (result == MessageBoxResult.Yes)
            {
                DeleteSoftware((SoftwareDto) SoftwaresListView.SelectedItem);
            }
        }

        private async void DeleteSoftware(SoftwareDto software)
        {
            StatusLabel.Content = "Delete software " + software.ToString();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", Constants.TokenModel.AccessToken));

                var responseMessage = await client.DeleteAsync("api/softwares/" + software.Id);
                if (!responseMessage.IsSuccessStatusCode)
                {
                    string responseError = await responseMessage.Content.ReadAsStringAsync();
                    _logger.Error(responseError);
                    return;
                }
                StatusLabel.Content = software.ToString() + " successfully deleted.";
            }
            GetSoftwares();
        }

        private void ExitMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void BtnNewSoftware_OnClick(object sender, RoutedEventArgs e)
        {
            BtnSaveEnabled(true);
            BtnNewLicenseEnabled(false);

            _newSoftware = true;
            SoftwaresListView.SelectedItem = null;
            _currentSoftwareDetailDto = null;
            

            SoftwareBaseGrid newSwGrid = new SoftwareBaseGrid();
            Grid.SetColumn(newSwGrid, 1);
            Grid.SetRow(newSwGrid, 1);
            ParentGrid.Children.Remove(_currentGrid);
            _currentGrid = newSwGrid;
            Refresh();
        }

        private void BtnSaveEnabled(bool enabled)
        {
            if (enabled)
            {
                BtnSave.IsEnabled = true;
                BtnSaveImage.Source = new BitmapImage(
                    new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/Actions-document-save-icon.png"));
                return;
            }
            BtnSave.IsEnabled = false;
            BtnSaveImage.Source = new BitmapImage(
                new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/Actions-document-save-icon_grau.png"));
        }


        private void BtnNewLicenseEnabled(bool enabled)
        {
            if (enabled)
            {
                BtnNewLicense.IsEnabled = true;
                BtnNewLicenseImage.Source = new BitmapImage(
                    new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/add-key-icon.png"));
                return;
            }
            BtnNewLicense.IsEnabled = false;
            BtnNewLicenseImage.Source = new BitmapImage(
                new Uri(@"pack://application:,,,/LicenseManager.Client;component/Icons/add-key-icon_grau.png"));
        }

        private void BtnNewLicense_OnClick(object sender, RoutedEventArgs e)
        {
            NewLicenseDialog dialog = new NewLicenseDialog(_currentSoftwareDetailDto);

            //bool? result = dialog.ShowDialog();
            dialog.ShowDialog();
            Thread.Sleep(1000);
            GetSoftwareDetails();
            
        }
    }
}
