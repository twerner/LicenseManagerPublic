﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using LicenseManager.Client.Models;

namespace LicenseManager.Client.Grids
{
    public class SoftwareBaseGrid : Grid
    {
        private Label _lblTitle;
        private Grid _subGrid;
        private readonly SoftwareDetailDto _software;


        /// <summary>
        /// creates software grid for entering new software
        /// </summary>
        public SoftwareBaseGrid()
        {
            Initialize();
        }

        /// <summary>
        /// constructor for display existing software
        /// </summary>
        /// <param name="software"></param>
        public SoftwareBaseGrid(SoftwareDetailDto software)
        {
            _software = software;
            Initialize();
        }

        private void CreateBaseGrid()
        {
            RowDefinitions.Add(new RowDefinition() {Height = GridLength.Auto});
            RowDefinitions.Add(new RowDefinition() {Height = new GridLength(25)});
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition());

            ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(25) });
            ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            ColumnDefinitions.Add(new ColumnDefinition());
        }

        private void Initialize()
        {
            CreateBaseGrid();
            var lblTitleContent = "Enter new software";
            if (null != _software)
            {
                lblTitleContent = _software.ToString();
                _subGrid = new SoftwareDetailGrid(_software);
            }
            else
            {
                //_subGrid = new SoftwareDetailGrid();
                //TODO new NewSoftwareGrid
            }
            var lblTitle = new Label()
            {
                Name = "lblTitle",
                Content = lblTitleContent,
                FontSize = 20,
                FontWeight = FontWeights.Bold
            };
            SetColumn(lblTitle, 0);
            SetRow(lblTitle, 0);
            SetColumnSpan(lblTitle, 3);
            Children.Add(lblTitle);

            if (null == _subGrid) return;
            SetRow(_subGrid, 2);
            SetColumn(_subGrid, 1);
            Children.Add(_subGrid);
        }

    }
}
