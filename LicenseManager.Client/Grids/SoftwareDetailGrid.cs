﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using LicenseManager.Client.Dialogs;
using LicenseManager.Client.Models;
using DataGrid = System.Windows.Controls.DataGrid;
using Label = System.Windows.Controls.Label;
using MessageBox = System.Windows.MessageBox;
using TextBox = System.Windows.Controls.TextBox;

namespace LicenseManager.Client.Grids
{
    public class SoftwareDetailGrid : Grid
    {
        private readonly SoftwareDetailDto _software;

        //Labels
        private Label _lblManufacturer;
        private Label _lblName;
        private Label _lblGenre;
        private Label _lblDescription;
        private Label _lblCreated;
        private Label _lblCreatedBy;

        //Textboxes
        private TextBox _txtMaufacturer;
        private TextBox _txtName;
        private TextBox _txtGenre;
        private TextBox _txtDesciption;
        private TextBox _txtCreated;
        private TextBox _txtCreatedBy;

        private DataGrid _licenseGrid;

        public SoftwareDetailGrid(SoftwareDetailDto software)
        {
            _software = software;
            Initialize();
        }

        private void Initialize()
        {
            ColumnDefinitions.Add(new ColumnDefinition() {Width = GridLength.Auto});
            ColumnDefinitions.Add(new ColumnDefinition()
            {
                Width = GridLength.Auto,
                MaxWidth = 400
            });

            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            RowDefinitions.Add(new RowDefinition());

            CreateLabels();
            CreateTextboxes();

            SetLicenseGrid();
        }
        
        private void SetLicenseGrid()
        {
            _licenseGrid = new DataGrid();
            _licenseGrid.ItemsSource = _software.Licenses;
            _licenseGrid.MouseDoubleClick += _licenseGrid_MouseDoubleClick;
            _licenseGrid.IsReadOnly = true;
            SetRow(_licenseGrid, 6);
            SetColumn(_licenseGrid, 0);
            SetColumnSpan(_licenseGrid, 2);
            Children.Add(_licenseGrid);
        }

        void _licenseGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            EditLicenseDialog dialog = new EditLicenseDialog((License)_licenseGrid.SelectedItem);
            dialog.ShowDialog();
        }

        private void CreateTextboxes()
        {
            _txtMaufacturer = new TextBox() { Text = _software.ManufacturerName };
            _txtName = new TextBox() { Text = _software.Name };
            _txtGenre = new TextBox() { Text = _software.Genre };
            _txtDesciption = new TextBox() { Text = _software.Description };
            _txtCreated = new TextBox() {Text = _software.Created.ToLongDateString()};
            _txtCreatedBy = new TextBox() {Text = _software.CreatedByUserName};

            SetColumn(_txtMaufacturer, 1);
            SetColumn(_txtName, 1);
            SetColumn(_txtGenre, 1);
            SetColumn(_txtDesciption, 1);
            SetColumn(_txtCreated, 1);
            SetColumn(_txtCreatedBy, 1);
            
            SetRow(_txtMaufacturer, 0);
            SetRow(_txtName, 1);
            SetRow(_txtGenre, 2);
            SetRow(_txtDesciption, 3);
            SetRow(_txtCreated, 4);
            SetRow(_txtCreatedBy, 5);
            
            Children.Add(_txtMaufacturer);
            Children.Add(_txtName);
            Children.Add(_txtGenre);
            Children.Add(_txtDesciption);
            Children.Add(_txtCreated);
            Children.Add(_txtCreatedBy);

        }

        private void CreateLabels()
        {
            _lblManufacturer = new Label()
            {
                Content = "Manufacturer"
            };
            SetColumn(_lblManufacturer, 0);
            SetRow(_lblManufacturer, 0);
            Children.Add(_lblManufacturer);

            _lblName = new Label()
            {
                Content = "Name"
            };
            SetColumn(_lblName, 0);
            SetRow(_lblName, 1);
            Children.Add(_lblName);

            _lblGenre = new Label()
            {
                Content = "Genre"
            };
            SetColumn(_lblGenre, 0);
            SetRow(_lblGenre, 2);
            Children.Add(_lblGenre);

            _lblDescription = new Label()
            {
                Content = "Description"
            };
            SetColumn(_lblDescription, 0);
            SetRow(_lblDescription, 3);
            Children.Add(_lblDescription);

            _lblCreated = new Label()
            {
                Content = "Created"
            };
            SetColumn(_lblCreated, 0);
            SetRow(_lblCreated, 4);
            Children.Add(_lblCreated);

            _lblCreatedBy = new Label()
            {
                Content = "Created by"
            };
            SetColumn(_lblCreatedBy, 0);
            SetRow(_lblCreatedBy, 5);
            Children.Add(_lblCreatedBy);
        }

    }
}
