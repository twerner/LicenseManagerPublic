﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace LicenseManager.Client.Grids
{
    public class WelcomeGrid : Grid
    {
        public WelcomeGrid(string name)
        {
            base.Name = name;
            Init();
        }

        private void Init()
        {
            RowDefinition row = new RowDefinition()
            {
                Height = GridLength.Auto
            };
            RowDefinition lastRow = new RowDefinition();
            RowDefinitions.Add(row);
            RowDefinitions.Add(lastRow);

            ColumnDefinitions.Add(new ColumnDefinition(){Width = GridLength.Auto});
            ColumnDefinitions.Add(new ColumnDefinition());

            Label label1 = new Label()
            {
                Content = "Test",
            };

            SetRow(label1, 0);
            SetColumn(label1, 0);
            this.Children.Add(label1);
        }
    }
}
