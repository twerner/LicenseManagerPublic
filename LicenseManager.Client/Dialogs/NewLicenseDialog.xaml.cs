﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LicenseManager.Client.Models;
using NLog;

namespace LicenseManager.Client.Dialogs
{
    /// <summary>
    /// Interaktionslogik für NewLicenseDialog.xaml
    /// </summary>
    public partial class NewLicenseDialog : Window
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();
        private SoftwareDetailDto _software;
        private License _license;
        private bool _success;

        public NewLicenseDialog(SoftwareDetailDto software)
        {
            InitializeComponent();
            _software = software;
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            _license = CreateLicense();
            
            Task postTask = new Task(PostLicense);

            postTask.Start();

            while (!postTask.IsCompleted)
            {
                PostProgressBar.Value++;
            }


            if (_success)
            {
                DialogResult = true;
            }

            DialogResult = false;
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private async void PostLicense()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", Constants.TokenModel.AccessToken));

                var response = await client.PostAsJsonAsync("api/licenses", _license).ConfigureAwait(false);
                if (!response.IsSuccessStatusCode)
                {
                    //var responseError = await response.Content.ReadAsStringAsync();
                    //_logger.Error(responseError);
                    _success = false;
                }
                _success = true;
            }
        }

        private License CreateLicense()
        {
            PostProgressBar.Value = 25;
            License license = new License()
            {
                SoftwareId =  _software.Id,
                Comment = txtComment.Text,
                Edition = txtEdition.Text,
                Key = txtKey.Text,
                UsedOn = txtUsedOn.Text
            };

            return license;
        }
    }
}
