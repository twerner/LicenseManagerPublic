﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LicenseManager.Client.Models;

namespace LicenseManager.Client.Dialogs
{
    /// <summary>
    /// Interaktionslogik für EditLicenseDialog.xaml
    /// </summary>
    public partial class EditLicenseDialog : Window
    {
        private License _license;
        private bool _success;


        public EditLicenseDialog(License license)
        {
            InitializeComponent();
            _license = license;

            InitializeTextBoxes();

        }

        private void InitializeTextBoxes()
        {
            txtComment.Text = _license.Comment;
            txtEdition.Text = _license.Edition;
            txtKey.Text = _license.Key;
            txtUsedOn.Text = _license.UsedOn;
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            UpdateLicense();

            Task putTask = new Task(PutLicense);

            putTask.Start();


            if (_success)
            {
                DialogResult = true;
            }

            DialogResult = false;
        }

        private void UpdateLicense()
        {
            _license.Comment = txtComment.Text;
            if (!String.IsNullOrEmpty(txtEdition.Text))
            {
                _license.Edition = txtEdition.Text;
            }
            _license.UsedOn = txtUsedOn.Text;
        }

        private async void PutLicense()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.BaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", Constants.TokenModel.AccessToken));

                var response = await client.PutAsJsonAsync("api/licenses/" + _license.Id, _license).ConfigureAwait(false);
                if (!response.IsSuccessStatusCode)
                {
                    //var responseError = await response.Content.ReadAsStringAsync();
                    //_logger.Error(responseError);
                    _success = false;
                }
                _success = true;
            }
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
