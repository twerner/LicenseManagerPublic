﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseManager.Client.Models
{
    public class SoftwareDetailDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        public string CreatedById { get; set; }
        public string CreatedByUserName { get; set; }
        public string LastModifiedById { get; set; }
        public string LastModifiedByUserName { get; set; }
        public int Version { get; set; }
        public ICollection<License> Licenses { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}", ManufacturerName, Name);
        }
    }
}
