﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseManager.Client.Models
{
    public class License
    {
        public int Id { get; set; }
        public int SoftwareId { get; set; }
        public string Key { get; set; }
        public string Edition { get; set; }
        public string Comment { get; set; }

        public string UsedOn { get; set; }
    }
}
